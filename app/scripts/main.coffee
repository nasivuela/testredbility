# Main menu controller
menu = ()->
		
	$(".navCtrl").on "click", (event)->
		event.preventDefault()
		if $(".pageWrapp").hasClass "phone"
			
			# Check if mainMenu or subMenu
			if $(this).hasClass("phoneNavCtrl")
				toOpen = $(".nav")
			else
				toOpen = $(this).next()
				
			menu =  $(".nav")
			subMenuControl = $(".navE")
				
			# Close mainMenu/subMenu
			if $(this).hasClass("open")
	
				# FadeOut cross
				$("#closeNav").addClass "hideS"
				
				# Remove open state
				$(this).removeClass "open"
				
				#Calculate height
				curHeight = toOpen.height()
				autoHeight = toOpen.css("height", "auto").height()
				
				# Close Menu
				toOpen.animate
						height: "0"
				, 300, ->
					toOpen.addClass("hide")
				
				# When menu closing is subMenu
				if $(this).hasClass("navE")
					
					# Deactive subMenu light
					$(this).children().children().children(".onTop").addClass("hideS")
					$(this).removeClass("navTxtAct")
					
					# Recalculate mainMenu height
					redimCurHeight = menu.height()
					redimAutoHeight = menu.css("height", "auto").height()	
					menu.height(redimCurHeight).animate
							height: redimAutoHeight + 54 - autoHeight
					, 300
				
				# When menu closing is mainMenu
				else
				
					# when subMenu is open, close it
					if subMenuControl.hasClass "open"
						subMenuControl.removeClass "open"
						subMenuControl.next().animate
							height: "0"
						, 300, ->
							subMenuControl.next().addClass("hide")
											
						# Deactive subMenu light
						subMenuControl.children().children().children(".onTop").addClass("hideS")
						subMenuControl.removeClass("navTxtAct")
							

					
		
			# Open mainMenu/subMenu
			else
				
				# FadeIn cross
				$("#closeNav").removeClass "hideS"
	
				# Add open state
				$(this).addClass "open"
				
				# Calculate height
				curHeight = toOpen.height()
				autoHeight = toOpen.css("height", "auto").height() 
				
				# When menu opening is mainMenu
				if $(this).hasClass("phoneNavCtrl")
				
					# Give top padding
					autoHeight += 54
					
				# When menu opening is subMenu
				else
					
					#Active subMenu light
					$(this).children().children().children(".onTop").removeClass("hideS")
					$(this).addClass("navTxtAct")
					
					# Recalculate height of mainMenu
					redimCurHeight = menu.height()
					redimAutoHeight = menu.css("height", "auto").height()	
					menu.height(redimCurHeight).animate
							height: redimAutoHeight + 54 + autoHeight
					, 300	
				
				# Open Menu			
				toOpen.removeClass("hide").height(curHeight).animate
						height: autoHeight
				, 300		
			return
		return

fadeCarrousel = ->

	#click on each control
	$(".crrslCtrl").each (index)->
		$(this).on "click", (event)->
			event.preventDefault()
			# Active menu
			$(".crrslWrapp").find(".crrslCtrlAct").removeClass("crrslCtrlAct")
			$(this).addClass("crrslCtrlAct")

			# Change Image

			crrslElems = $(".crrslWrapp").children(".crrslE")
			crrslEOut = $(".crrslWrapp").find(".crrslAct")

			crrslEOut.addClass("crrslOut").removeClass("crrslAct")
			$(crrslElems[index]).addClass("crrslAct")
			setTimeout (->
				  crrslEOut.removeClass("crrslOut")
				  return
			), 3000


# Carrousel height controller
resize = ->
	
	# set Height on carrousel elements
	setHeight = ->
		windowHeight = $(window).height()
		$(".crrslE, .crrslWrapp").css("height", windowHeight + "px")

	# set Height on carrousel elements on resizes
	setHeighOnResize = ->
		$(window).resize ->
			setHeight() unless $(".pageWrapp").hasClass "phone"
	
	widthChange = (mq)->

		# if min-width is 768px (TABLE & WEB VIEW)
		if mq.matches
			setHeight()
			setHeighOnResize()

			# Remove phone status from nav
			$(".pageWrapp").removeClass "phone" if $(".pageWrapp").hasClass "phone"
			
			# Reset nav #
			# Close nav and subNav
			if $(".phoneNavCtrl").hasClass "open"
					$(".phoneNavCtrl").removeClass "open"
					# FadeOut cross
					$("#closeNav").addClass "hideS"

			if $(".navE").hasClass "open"
				$(".navE").removeClass "open"
				# Deactive subMenu light
				$(".navE").children().children().children(".onTop").addClass("hideS")
				$(".navE").removeClass("navTxtAct")

			# Clean nav height (css & inline)
			$(".nav, .subNav").removeClass("hide noHeight")

			if $(".nav").attr("style") isnt undefined
				$(".nav").removeAttr "style"

			$(".subNav").each ()->
				if $(this).attr("style") isnt undefined
					$(this).removeAttr "style"

			
		# if min-width isnt 768px (PHONE VIEW)
		else
			
			# Add phone status from nav
			$(".pageWrapp").addClass "phone" unless $(".pageWrapp").hasClass "phone"
			
			# Reset height
			unless $("nav").hasClass("hide")
				$(".nav, .subNav").addClass("hide noHeight")
			

			if $(".crrslWrapp").attr("style") isnt undefined
				$(".crrslWrapp").removeAttr("style")

			setTimeout (->
				$(".crrslE").each ()->
					if $(this).attr("style") isnt undefined
						$(this).removeAttr "style"
			), 100

	
	# Media listener  
	if (window).matchMedia 
		mq = (window).matchMedia("(min-width: 768px)")
		mq.addListener widthChange
		widthChange mq

# On document ready, let the magic happen
$(document).ready ->
	menu()
	resize()
	fadeCarrousel()
