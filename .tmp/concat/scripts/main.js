(function() {
  var fadeCarrousel, menu, resize;

  menu = function() {
    return $(".navCtrl").on("click", function(event) {
      var autoHeight, curHeight, redimAutoHeight, redimCurHeight, subMenuControl, toOpen;
      event.preventDefault();
      if ($(".pageWrapp").hasClass("phone")) {
        if ($(this).hasClass("phoneNavCtrl")) {
          toOpen = $(".nav");
        } else {
          toOpen = $(this).next();
        }
        menu = $(".nav");
        subMenuControl = $(".navE");
        if ($(this).hasClass("open")) {
          $("#closeNav").addClass("hideS");
          $(this).removeClass("open");
          curHeight = toOpen.height();
          autoHeight = toOpen.css("height", "auto").height();
          toOpen.animate({
            height: "0"
          }, 300, function() {
            return toOpen.addClass("hide");
          });
          if ($(this).hasClass("navE")) {
            $(this).children().children().children(".onTop").addClass("hideS");
            $(this).removeClass("navTxtAct");
            redimCurHeight = menu.height();
            redimAutoHeight = menu.css("height", "auto").height();
            menu.height(redimCurHeight).animate({
              height: redimAutoHeight + 54 - autoHeight
            }, 300);
          } else {
            if (subMenuControl.hasClass("open")) {
              subMenuControl.removeClass("open");
              subMenuControl.next().animate({
                height: "0"
              }, 300, function() {
                return subMenuControl.next().addClass("hide");
              });
              subMenuControl.children().children().children(".onTop").addClass("hideS");
              subMenuControl.removeClass("navTxtAct");
            }
          }
        } else {
          $("#closeNav").removeClass("hideS");
          $(this).addClass("open");
          curHeight = toOpen.height();
          autoHeight = toOpen.css("height", "auto").height();
          if ($(this).hasClass("phoneNavCtrl")) {
            autoHeight += 54;
          } else {
            $(this).children().children().children(".onTop").removeClass("hideS");
            $(this).addClass("navTxtAct");
            redimCurHeight = menu.height();
            redimAutoHeight = menu.css("height", "auto").height();
            menu.height(redimCurHeight).animate({
              height: redimAutoHeight + 54 + autoHeight
            }, 300);
          }
          toOpen.removeClass("hide").height(curHeight).animate({
            height: autoHeight
          }, 300);
        }
        return;
      }
    });
  };

  fadeCarrousel = function() {
    return $(".crrslCtrl").each(function(index) {
      return $(this).on("click", function(event) {
        var crrslEOut, crrslElems;
        event.preventDefault();
        $(".crrslWrapp").find(".crrslCtrlAct").removeClass("crrslCtrlAct");
        $(this).addClass("crrslCtrlAct");
        crrslElems = $(".crrslWrapp").children(".crrslE");
        crrslEOut = $(".crrslWrapp").find(".crrslAct");
        crrslEOut.addClass("crrslOut").removeClass("crrslAct");
        $(crrslElems[index]).addClass("crrslAct");
        return setTimeout((function() {
          crrslEOut.removeClass("crrslOut");
        }), 3000);
      });
    });
  };

  resize = function() {
    var mq, setHeighOnResize, setHeight, widthChange;
    setHeight = function() {
      var windowHeight;
      windowHeight = $(window).height();
      return $(".crrslE, .crrslWrapp").css("height", windowHeight + "px");
    };
    setHeighOnResize = function() {
      return $(window).resize(function() {
        if (!$(".pageWrapp").hasClass("phone")) {
          return setHeight();
        }
      });
    };
    widthChange = function(mq) {
      if (mq.matches) {
        setHeight();
        setHeighOnResize();
        if ($(".pageWrapp").hasClass("phone")) {
          $(".pageWrapp").removeClass("phone");
        }
        if ($(".phoneNavCtrl").hasClass("open")) {
          $(".phoneNavCtrl").removeClass("open");
          $("#closeNav").addClass("hideS");
        }
        if ($(".navE").hasClass("open")) {
          $(".navE").removeClass("open");
          $(".navE").children().children().children(".onTop").addClass("hideS");
          $(".navE").removeClass("navTxtAct");
        }
        $(".nav, .subNav").removeClass("hide noHeight");
        if ($(".nav").attr("style") !== void 0) {
          $(".nav").removeAttr("style");
        }
        return $(".subNav").each(function() {
          if ($(this).attr("style") !== void 0) {
            return $(this).removeAttr("style");
          }
        });
      } else {
        if (!$(".pageWrapp").hasClass("phone")) {
          $(".pageWrapp").addClass("phone");
        }
        if (!$("nav").hasClass("hide")) {
          $(".nav, .subNav").addClass("hide noHeight");
        }
        if ($(".crrslWrapp").attr("style") !== void 0) {
          $(".crrslWrapp").removeAttr("style");
        }
        return setTimeout((function() {
          return $(".crrslE").each(function() {
            if ($(this).attr("style") !== void 0) {
              return $(this).removeAttr("style");
            }
          });
        }), 100);
      }
    };
    if (window.matchMedia) {
      mq = window.matchMedia("(min-width: 768px)");
      mq.addListener(widthChange);
      return widthChange(mq);
    }
  };

  $(document).ready(function() {
    menu();
    resize();
    return fadeCarrousel();
  });

}).call(this);
